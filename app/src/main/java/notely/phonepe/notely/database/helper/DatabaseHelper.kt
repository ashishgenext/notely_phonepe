package notely.phonepe.notely.database.helper

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import notely.phonepe.notely.database.contract.DbContract
import notely.phonepe.notely.model.ListItemModel

/**
 * Created by Ashish on 29-01-2018.
 */

class DatabaseHelper(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {
    companion object {
        val DB_NAME: String = "notes.db"
        val DB_VERSION: Int = 1
        private val SQL_CREATE_ENTRIES =
                "CREATE TABLE " + DbContract.UserEntry.TABLE_NAME + " (" +
                        DbContract.UserEntry.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                        DbContract.UserEntry.COLUMN_TITLE + " TEXT," +
                        DbContract.UserEntry.COLUMN_SUB_TITLE + " TEXT," +
                        DbContract.UserEntry.COLUMN_CONTENT + " TEXT," +
                        DbContract.UserEntry.COLUMN_TIMESTAMP + " TEXT," +
                        DbContract.UserEntry.COLUMN_DELETE_STATUS + " INTEGER," +
                        DbContract.UserEntry.COLUMN_FAVOURITE + " INTEGER," +
                        DbContract.UserEntry.COLUMN_LIKE + " INTEGER)"

        private val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + DbContract.UserEntry.TABLE_NAME

    }

    override fun onCreate(p0: SQLiteDatabase?) {
        p0!!.execSQL(SQL_CREATE_ENTRIES)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        p0!!.execSQL(SQL_DELETE_ENTRIES)
        onCreate(p0)
    }


    fun insertNote(model: ListItemModel) {
        var db = writableDatabase

        val values = ContentValues()
        values.put(DbContract.UserEntry.COLUMN_TITLE, model.mTitle)
        values.put(DbContract.UserEntry.COLUMN_SUB_TITLE, model.mSubtitle)
        values.put(DbContract.UserEntry.COLUMN_CONTENT, model.mContent)
        values.put(DbContract.UserEntry.COLUMN_FAVOURITE, if (model.mFavourite) 1 else 0)
        values.put(DbContract.UserEntry.COLUMN_LIKE, if (model.mLike) 1 else 0)
        values.put(DbContract.UserEntry.COLUMN_TIMESTAMP, model.mTimestamp)
        values.put(DbContract.UserEntry.COLUMN_DELETE_STATUS, if (model.mDeleteStatus) 1 else 0)

        var i = db.insert(DbContract.UserEntry.TABLE_NAME, null, values)


    }

    fun getNote(id: String): ListItemModel? {
        var model: ListItemModel? = null
        val db = writableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery("select * from " + DbContract.UserEntry.TABLE_NAME + " WHERE " + DbContract.UserEntry.COLUMN_DELETE_STATUS + "='0' AND " + DbContract.UserEntry.COLUMN_ID + "='" + id + "'", null)
        } catch (e: SQLiteException) {
            // if table not yet present, create it
            return null
        }

        if (cursor!!.moveToFirst()) {
            while (!cursor.isAfterLast) {
                model = ListItemModel().apply {
                    mTitle = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_TITLE))
                    mSubtitle = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_SUB_TITLE))
                    mContent = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_CONTENT))
                    mId = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_ID))
                    mLike = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_LIKE)) == 1
                    mFavourite = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_FAVOURITE)) == 1
                    mDeleteStatus = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_DELETE_STATUS)) == 0
                    mTimestamp = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_TIMESTAMP))
                }
                cursor.moveToNext()
            }
        }
        return model
    }

    fun getAllNotes(): ArrayList<ListItemModel>? {
        var model = ListItemModel()
        var list = ArrayList<ListItemModel>()

        val db = writableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery("select * from " + DbContract.UserEntry.TABLE_NAME + " WHERE " + DbContract.UserEntry.COLUMN_DELETE_STATUS + "='0'", null)
        } catch (e: SQLiteException) {
            // if table not yet present, create it
            return null
        }

        if (cursor!!.moveToFirst()) {
            while (!cursor.isAfterLast) {
                model = ListItemModel().apply {
                    mTitle = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_TITLE))
                    mSubtitle = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_SUB_TITLE))
                    mContent = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_CONTENT))
                    mId = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_ID))
                    mLike = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_LIKE)) == 1
                    mFavourite = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_FAVOURITE)) == 1
                    mDeleteStatus = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_DELETE_STATUS)) == 0
                    mTimestamp = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_TIMESTAMP))
                }
                list.add(model)
                cursor.moveToNext()
            }
        }
        return list
    }

    fun getAllFavourite(): ArrayList<ListItemModel>? {
        var model = ListItemModel()
        var list = ArrayList<ListItemModel>()

        val db = writableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery("select * from " + DbContract.UserEntry.TABLE_NAME + " WHERE " + DbContract.UserEntry.COLUMN_DELETE_STATUS + "='0' AND " + DbContract.UserEntry.COLUMN_FAVOURITE + "='1' AND " + DbContract.UserEntry.COLUMN_LIKE + "='0'", null)
        } catch (e: SQLiteException) {
            // if table not yet present, create it
            return null
        }

        if (cursor!!.moveToFirst()) {
            while (!cursor.isAfterLast) {
                model = ListItemModel().apply {
                    mTitle = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_TITLE))
                    mSubtitle = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_SUB_TITLE))
                    mContent = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_CONTENT))
                    mId = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_ID))
                    mLike = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_LIKE)) == 1
                    mFavourite = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_FAVOURITE)) == 1
                    mDeleteStatus = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_DELETE_STATUS)) == 1
                    mTimestamp = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_TIMESTAMP))
                }
                list.add(model)
                cursor.moveToNext()
            }
        }
        return list
    }

    fun getAllLike(): ArrayList<ListItemModel>? {
        var model = ListItemModel()
        var list = ArrayList<ListItemModel>()

        val db = writableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery("select * from " + DbContract.UserEntry.TABLE_NAME + " WHERE " + DbContract.UserEntry.COLUMN_DELETE_STATUS + "='0' AND " + DbContract.UserEntry.COLUMN_FAVOURITE + "='0' AND " + DbContract.UserEntry.COLUMN_LIKE + "='1'", null)
        } catch (e: SQLiteException) {
            // if table not yet present, create it
            return null
        }

        if (cursor!!.moveToFirst()) {
            while (!cursor.isAfterLast) {
                model = ListItemModel().apply {
                    mTitle = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_TITLE))
                    mSubtitle = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_SUB_TITLE))
                    mContent = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_CONTENT))
                    mId = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_ID))
                    mLike = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_LIKE)) == 1
                    mFavourite = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_FAVOURITE)) == 1
                    mDeleteStatus = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_DELETE_STATUS)) == 1
                    mTimestamp = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_TIMESTAMP))
                }
                list.add(model)
                cursor.moveToNext()
            }
        }
        return list
    }

    fun getAllLikeAndFavourite(): ArrayList<ListItemModel>? {
        var model = ListItemModel()
        var list = ArrayList<ListItemModel>()

        val db = writableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery("select * from " + DbContract.UserEntry.TABLE_NAME + " WHERE " + DbContract.UserEntry.COLUMN_DELETE_STATUS + "='0' AND " + DbContract.UserEntry.COLUMN_FAVOURITE + "='1' AND " + DbContract.UserEntry.COLUMN_LIKE + "='1'", null)
        } catch (e: SQLiteException) {
            // if table not yet present, create it
            return null
        }

        if (cursor!!.moveToFirst()) {
            while (!cursor.isAfterLast) {
                model = ListItemModel().apply {
                    mTitle = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_TITLE))
                    mSubtitle = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_SUB_TITLE))
                    mContent = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_CONTENT))
                    mId = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_ID))
                    mLike = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_LIKE)) == 1
                    mFavourite = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_FAVOURITE)) == 1
                    mDeleteStatus = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_DELETE_STATUS)) == 1
                    mTimestamp = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_TIMESTAMP))
                }
                list.add(model)
                cursor.moveToNext()
            }
        }
        return list
    }

    fun deleteNote(id: Int) {
        val db = writableDatabase

        val values = ContentValues()
        values.put(DbContract.UserEntry.COLUMN_DELETE_STATUS, 1)
        db.update(DbContract.UserEntry.TABLE_NAME, values, DbContract.UserEntry.COLUMN_ID + "= ?", arrayOf(id.toString()))
    }

    fun updateLike(id: Int, like: Int) {
        val db = writableDatabase

        val values = ContentValues()
        values.put(DbContract.UserEntry.COLUMN_LIKE, like)
        db.update(DbContract.UserEntry.TABLE_NAME, values, DbContract.UserEntry.COLUMN_ID + "= ?", arrayOf(id.toString()))
    }

    fun updateFavourite(id: Int, fav: Int) {
        val db = writableDatabase

        val values = ContentValues()
        values.put(DbContract.UserEntry.COLUMN_FAVOURITE, fav)
        db.update(DbContract.UserEntry.TABLE_NAME, values, DbContract.UserEntry.COLUMN_ID + "= ?", arrayOf(id.toString()))
    }

    fun updateNote(id: Int, model: ListItemModel) {

        var db = writableDatabase

        val values = ContentValues()
        values.put(DbContract.UserEntry.COLUMN_TITLE, model.mTitle)
        values.put(DbContract.UserEntry.COLUMN_SUB_TITLE, model.mSubtitle)
        values.put(DbContract.UserEntry.COLUMN_CONTENT, model.mContent)
        values.put(DbContract.UserEntry.COLUMN_TIMESTAMP, model.mTimestamp)

        db.update(DbContract.UserEntry.TABLE_NAME, values, DbContract.UserEntry.COLUMN_ID + "= ?", arrayOf(id.toString()))
    }

    fun getIdWithTimeStamp(time: String): ListItemModel? {
        var model: ListItemModel? = null
        val db = writableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery("select * from " + DbContract.UserEntry.TABLE_NAME + " WHERE " + DbContract.UserEntry.COLUMN_DELETE_STATUS + "='0' AND " + DbContract.UserEntry.COLUMN_TIMESTAMP + "='" + time + "'", null)
        } catch (e: SQLiteException) {
            // if table not yet present, create it
            return null
        }

        if (cursor!!.moveToFirst()) {
            while (!cursor.isAfterLast) {
                model = ListItemModel().apply {
                    mTitle = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_TITLE))
                    mSubtitle = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_SUB_TITLE))
                    mContent = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_CONTENT))
                    mId = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_ID))
                    mLike = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_LIKE)) == 1
                    mFavourite = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_FAVOURITE)) == 1
                    mDeleteStatus = cursor.getInt(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_DELETE_STATUS)) == 0
                    mTimestamp = cursor.getString(cursor.getColumnIndex(DbContract.UserEntry.COLUMN_TIMESTAMP))
                }
                cursor.moveToNext()
            }
        }
        return model
    }


}
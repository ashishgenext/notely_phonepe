package notely.phonepe.notely.database.contract

import android.provider.BaseColumns

/**
 * Created by Ashish on 29-01-2018.
 */
object DbContract {
    class UserEntry : BaseColumns {
        companion object {
            val TABLE_NAME = "note_table"
            val COLUMN_ID = "id"
            val COLUMN_TITLE = "title"
            val COLUMN_SUB_TITLE = "subtitle"
            val COLUMN_CONTENT = "content"
            val COLUMN_LIKE = "like"
            val COLUMN_FAVOURITE = "favourite"
            val COLUMN_TIMESTAMP = "timestamp"
            val COLUMN_DELETE_STATUS = "delete_status"
        }
    }
}
package notely.phonepe.notely.activity

import android.content.Intent
import android.graphics.Canvas
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.*
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import notely.phonepe.notely.R
import notely.phonepe.notely.adapter.NoteListAdapter
import notely.phonepe.notely.database.helper.DatabaseHelper
import notely.phonepe.notely.helper.OnListItemClickListener
import notely.phonepe.notely.helper.SwipeControllerActions
import notely.phonepe.notely.helper.SwipeTouchDrawer
import notely.phonepe.notely.model.ListItemModel

class MainActivity : AppCompatActivity(),OnListItemClickListener ,NavigationView.OnNavigationItemSelectedListener {

    var itemList: MutableList<ListItemModel>? = null
    var favSelected : Boolean = false
    var likeSelected : Boolean = false

    companion object {
        var dbHelper : DatabaseHelper? = null
        var mAdapter: NoteListAdapter? = null

    }

    init {
        var a = ListItemModel()
        a.mTitle = "AShish Singh"
        a.mSubtitle = "Engineer"
        a.mTimestamp = "Today at 6:30PM"
        a.mFavourite = true
        a.mLike = true
        itemList = mutableListOf(a, a, a, a, a, a, a, a, a, a, a, a, a)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dbHelper = DatabaseHelper(this)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        supportActionBar?.hide()

        var recyclerView: RecyclerView = findViewById(R.id.note_list)
        setupRecyclerView(recyclerView)

        filter_icon.setOnClickListener {
            if(side_bar.visibility == View.GONE){
                side_bar.visibility = View.VISIBLE
            }else{
                side_bar.visibility = View.GONE
            }
        }

        add_note_icon.setOnClickListener{
            var intent = Intent(this ,EditNoteActivity::class.java)
            intent.putExtra("isFromAddButton",true)
            startActivity(intent)
        }

        apply_button.setOnClickListener{
            Toast.makeText(this ,"filters applied",Toast.LENGTH_SHORT).show()
            when{
                likeSelected && favSelected -> {
                    dot.visibility = View.VISIBLE
                    mAdapter!!.itemList = dbHelper!!.getAllLikeAndFavourite()
                    mAdapter!!.notifyDataSetChanged()
                }
               !likeSelected && favSelected -> {
                   dot.visibility = View.VISIBLE
                    mAdapter!!.itemList = dbHelper!!.getAllFavourite()
                    mAdapter!!.notifyDataSetChanged()
                }
                likeSelected && !favSelected -> {
                    dot.visibility = View.VISIBLE
                    mAdapter!!.itemList = dbHelper!!.getAllLike()
                    mAdapter!!.notifyDataSetChanged()
                }
                !likeSelected && !favSelected -> {
                    dot.visibility = View.GONE
                    mAdapter!!.itemList = dbHelper!!.getAllNotes()
                    mAdapter!!.notifyDataSetChanged()
                }
            }
            side_bar.visibility = View.GONE
        }

        hearted.setOnClickListener{
            if(!likeSelected){
                likeSelected = true
                hearted_text.setTextColor(Color.parseColor("#50D3B5"))
                hearted_img.setImageResource(R.drawable.ic_check_green_24dp)
            }else{
                likeSelected = false
                hearted_text.setTextColor(Color.parseColor("#ffffff"))
                hearted_img.setImageResource(R.drawable.ic_check_grey_24dp)
            }
        }

        favourite.setOnClickListener {
            if(!favSelected){
                favSelected = true
                favourite_text.setTextColor(Color.parseColor("#50D3B5"))
                favourite_img.setImageResource(R.drawable.ic_check_green_24dp)
            }else{
                favSelected = false
                favourite_text.setTextColor(Color.parseColor("#ffffff"))
                favourite_img.setImageResource(R.drawable.ic_check_grey_24dp)
            }
        }
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {

        mAdapter = NoteListAdapter(this, dbHelper?.getAllNotes()!!,this)

        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.adapter = mAdapter
        recyclerView.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                startActivity(Intent(this@MainActivity,EditNoteActivity::class.java))
            }
        })

        var swipeController = SwipeTouchDrawer(swipeAction,this)

        val itemTouchHelper = ItemTouchHelper(swipeController)
        itemTouchHelper.attachToRecyclerView(recyclerView)

        recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State?) {
                swipeController.onDraw(c)
            }
        })
    }

    private var swipeAction = object : SwipeControllerActions(){
        override fun onRightClicked(position: Int) {
            dbHelper?.deleteNote(mAdapter!!.itemList!![position].mId!!)
            mAdapter?.removeItem(position)
            Toast.makeText(this@MainActivity,"Deleted",Toast.LENGTH_LONG).show()
        }

    }

    override fun onItemClick(view: View?,position : Int) {
        var intent = Intent(this ,EditNoteActivity::class.java)
        val id : Int = view?.getTag(R.string.tag_id)?.toString()!!.toInt()
        intent.putExtra("isFromAddButton",false)
        intent.putExtra("id",id)
        intent.putExtra("position",position)
        intent.putExtra("timestamp", mAdapter!!.getFormattedDate(this ,mAdapter!!.itemList!![position].mTimestamp!!.toLong()))
        startActivity(intent)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(Gravity.RIGHT)) {
            drawer_layout.closeDrawer(Gravity.RIGHT)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        return true
    }


}

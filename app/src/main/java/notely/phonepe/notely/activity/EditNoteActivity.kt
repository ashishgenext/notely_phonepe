package notely.phonepe.notely.activity

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.text.TextUtils
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_edit_note.*
import notely.phonepe.notely.R
import notely.phonepe.notely.model.ListItemModel

class EditNoteActivity : AppCompatActivity() {

    private var switchEditAndUndo : Boolean = false
    private var isFromAddButton : Boolean = false
    private var id : Int = -1
    private var position : Int = -1
    private var mNoteTimeStamp : String  = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_edit_note)

         isFromAddButton = intent.getBooleanExtra("isFromAddButton", false)
        if(isFromAddButton){
            switchEditAndUndo = true
            openEditMode()
        }else {
            switchEditAndUndo = false
            id = intent.getIntExtra("id",-1)
            mNoteTimeStamp = intent.getStringExtra("timestamp")
            position = intent.getIntExtra("position",-1)
            var model: ListItemModel? = MainActivity.dbHelper?.getNote(id.toString())
            note_title.setText(if(!TextUtils.isEmpty(model?.mTitle!!)) model?.mTitle else " ")
            note_content.setText(if(!TextUtils.isEmpty(model?.mContent!!)) model?.mContent else " ")
            note_time_stamp.setText("Last updated on $mNoteTimeStamp")
            openReadMode()
        }

        supportActionBar?.hide()

        back_button.setOnClickListener {
            if(isFromAddButton) saveToDb() else updateToDb(id)
            this@EditNoteActivity.finish()
        }


        save_and_edit.setOnClickListener {
            if (!switchEditAndUndo) {
                switchEditAndUndo = true
                openEditMode()
            } else {
                switchEditAndUndo = false
                openReadMode()
            }

            undo.setOnClickListener {
                if (switchEditAndUndo) {
                    switchEditAndUndo = false
                    openReadMode()
                }
            }

        }
    }

    private fun saveToDb(){
        if(TextUtils.isEmpty(note_title.text.toString())){
            Toast.makeText(this,"Note cannot be Saved ,Title Empty",Toast.LENGTH_SHORT).show()
            return
        }
        var model = ListItemModel()
        model.apply {
            mContent = note_content.text.toString()
            mTitle = note_title.text.toString()
            mSubtitle = ""
            mTimestamp = System.currentTimeMillis().toString()
            mFavourite = false
            mLike = false
            mDeleteStatus = false
        }
        MainActivity.dbHelper?.insertNote(model)
        MainActivity.mAdapter?.itemAdded(model)
        Toast.makeText(this,"Note Saved",Toast.LENGTH_SHORT).show()
    }

    private fun updateToDb(id : Int){
        if(TextUtils.isEmpty(note_title.text.toString())){
            Toast.makeText(this,"Note cannot be Saved ,Title Empty",Toast.LENGTH_SHORT).show()
            return
        }
        var model = ListItemModel()
        model.apply {
            mContent = note_content.text.toString()
            mTitle = note_title.text.toString()
            mSubtitle = ""
            mTimestamp = System.currentTimeMillis().toString()
        }
        MainActivity.dbHelper?.updateNote(id,model)
        if(position != -1) {
            MainActivity.mAdapter?.itemChanged(position , model)
        }
        Toast.makeText(this,"Note updated",Toast.LENGTH_SHORT).show()
    }

    override fun onBackPressed() {
        if(isFromAddButton) saveToDb() else updateToDb(id)
        super.onBackPressed()
    }

    private fun openReadMode(){
        undo.visibility = View.INVISIBLE
        note_title.background= ColorDrawable(Color.parseColor("#F5F5F5"))
        save_and_edit.text = "Edit"
        note_title.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_MULTI_LINE or InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
        note_title.isFocusableInTouchMode = false
        note_title.isClickable = false
        note_title.isCursorVisible = false

        note_content.inputType = InputType.TYPE_NULL
        note_content.isFocusableInTouchMode = false
        note_content.isClickable = false
        note_content.isCursorVisible = false

    }

    private fun openEditMode(){
        note_time_stamp.visibility = View.GONE
        undo.visibility = View.VISIBLE
        save_and_edit.text = "Save"
        note_title.isFocusableInTouchMode = true
        note_title.isClickable = true
        note_title.isCursorVisible = true
        note_title.background= ColorDrawable(Color.parseColor("#FFFFFF"))

        note_content.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_MULTI_LINE or InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
        note_content.isFocusableInTouchMode = true
        note_content.isClickable = true
        note_content.isCursorVisible = true
    }
}

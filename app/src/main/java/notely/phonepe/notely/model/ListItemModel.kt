package notely.phonepe.notely.model

/**
 * Created by Ashish on 27-01-2018.
 */
class ListItemModel {
    public var mTitle: String? = null
    public var mSubtitle: String? = null
    public var mContent: String? = null
    public var mTimestamp: String? = null
    public var mLike: Boolean = false
    public var mFavourite: Boolean = false
    public var mDeleteStatus : Boolean = false
    public var mId : Int? = null
}
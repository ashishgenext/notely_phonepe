package notely.phonepe.notely.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import notely.phonepe.notely.R
import notely.phonepe.notely.activity.MainActivity
import notely.phonepe.notely.helper.OnListItemClickListener
import notely.phonepe.notely.model.ListItemModel
import java.util.*

/**
 * Created by Ashish on 27-01-2018.
 */
class NoteListAdapter(context: Context, list: MutableList<ListItemModel>,listener: OnListItemClickListener) : RecyclerView.Adapter<NoteListAdapter.MyViewHolder>() {

    var itemList : MutableList<ListItemModel>? = null
    var context : Context? = null
    var mLikeflag : Boolean = false
    var mFavouriteFlag : Boolean = false
    var onListItemClick : OnListItemClickListener? = null
    init {
        this.itemList = list
        this.context = context
        this.onListItemClick = listener
    }

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int) {
        var item: ListItemModel = itemList!![position]
        holder!!.title!!.text = item!!.mTitle
        holder!!.subTitle!!.text = item!!.mContent?.subSequence(0,Math.min(if(item!!.mContent!!.length -1 < 0) 0 else item!!.mContent!!.length,15))
        holder!!.timeStamp!!.text = getFormattedDate(context!!,item.mTimestamp!!.toLong())
        if (item!!.mLike) {
            holder!!.likeButton!!.setImageResource(R.drawable.ic_heart_red)
            mLikeflag = true
        } else {
            mLikeflag = false
            holder!!.likeButton!!.setImageResource(R.drawable.ic_heart_grey)
        }
        if (item!!.mFavourite) {
            mFavouriteFlag = true
            holder!!.favouriteButton!!.setImageResource(R.drawable.ic_star_yellow_24dp)
        } else {
            mFavouriteFlag = false
            holder!!.favouriteButton!!.setImageResource(R.drawable.ic_star_grey_24dp)
        }

        holder.itemView.setTag(R.string.tag_id,itemList!![position].mId)
        holder.likeButton?.setOnClickListener{
            MainActivity.dbHelper?.updateLike(itemList!![position].mId!!,if(mLikeflag) 0 else 1)
            if(mLikeflag){
                mLikeflag = false
                holder.likeButton!!.setImageResource(R.drawable.ic_heart_grey)
            }else{
                mLikeflag = true
                holder.likeButton!!.setImageResource(R.drawable.ic_heart_red)
            }
        }

        holder.favouriteButton?.setOnClickListener{
            MainActivity.dbHelper?.updateFavourite(itemList!![position].mId!!,if(mFavouriteFlag) 0 else 1)
            if(mFavouriteFlag){
                mFavouriteFlag = false
                holder.favouriteButton!!.setImageResource(R.drawable.ic_star_grey_24dp)
            }else{
                mFavouriteFlag = true
                holder.favouriteButton!!.setImageResource(R.drawable.ic_star_yellow_24dp)
            }
        }
    }

     fun getFormattedDate(context: Context, smsTimeInMilis: Long): String {
        val smsTime = Calendar.getInstance()
        smsTime.timeInMillis = smsTimeInMilis

        val now = Calendar.getInstance()

        val timeFormatString = "h:mm aa"
        val dateTimeFormatString = "EEEE, MMMM d, h:mm aa"
        val HOURS = (60 * 60 * 60).toLong()
        return when {
            now.get(Calendar.DATE) === smsTime.get(Calendar.DATE) -> "Today " + DateFormat.format(timeFormatString, smsTime)
            now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) === 1 -> "Yesterday " + DateFormat.format(timeFormatString, smsTime)
            now.get(Calendar.YEAR) === smsTime.get(Calendar.YEAR) -> DateFormat.format(dateTimeFormatString, smsTime).toString()
            else -> DateFormat.format("MMMM dd yyyy, h:mm aa", smsTime).toString()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MyViewHolder {
        var itemView : View = LayoutInflater.from(parent!!.context).inflate(R.layout.note_list_item,parent,false)
        return  MyViewHolder(context,onListItemClick,itemView);
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }


    fun removeItem(position: Int){
        itemList!!.removeAt(position)
        notifyItemRemoved(position)
    }

    fun itemChanged(position: Int,model : ListItemModel){
        itemList!![position].apply {
            mTitle = model.mTitle
            mContent = model.mContent
            mTimestamp = model.mTimestamp
        }
        notifyItemChanged(position)
    }

    fun itemAdded(model : ListItemModel){
        model.mId =  MainActivity.dbHelper?.getIdWithTimeStamp(model.mTimestamp!!)?.mId
        itemList!!.add(model)
        notifyItemInserted(itemList!!.size)

    }




    class MyViewHolder(context : Context? ,listener: OnListItemClickListener?,itemView: View?) : RecyclerView.ViewHolder(itemView) ,View.OnClickListener {


        var title: TextView? = null
        var subTitle: TextView? = null
        var timeStamp: TextView? = null
        var favouriteButton: ImageView? = null
        var likeButton: ImageView? = null
        var deleteButton: ImageView? = null
        var viewBackground: RelativeLayout? = null
        var viewForeground: RelativeLayout? = null
        var listener : OnListItemClickListener? = null

        init {
            itemView?.setOnClickListener(this)
            likeButton = itemView!!.findViewById(R.id.like_button)
            favouriteButton = itemView!!.findViewById(R.id.star_button)
            timeStamp = itemView!!.findViewById(R.id.timestamp)
            subTitle = itemView!!.findViewById(R.id.subtitle)
            title = itemView!!.findViewById(R.id.title)
            viewForeground = itemView.findViewById(R.id.item_foreground);
            this.listener = listener
        }

        override fun onClick(v: View?) {
            listener?.onItemClick(v,adapterPosition)
        }



    }

}
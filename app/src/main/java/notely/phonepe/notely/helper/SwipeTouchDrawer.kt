package notely.phonepe.notely.helper

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper.*
import android.view.MotionEvent
import android.view.View
import notely.phonepe.notely.R

/**
 * Created by Ashish on 3/2/18.
 */

class SwipeTouchDrawer(controller: SwipeControllerActions, context: Context) : Callback() {

    private var swipeBack = false

    private var buttonShowedState = Button_canvas.GONE

    private var buttonInstance: RectF? = null

    private var currentItemViewHolder: RecyclerView.ViewHolder? = null

    private var buttonsActions: SwipeControllerActions? = null
    private var context: Context? = null

    private val buttonWidth = 300f

    init {
        this.buttonsActions = controller
        this.context = context
    }


    override fun getMovementFlags(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?): Int {
        return makeMovementFlags(0, LEFT)
    }

    override fun onMove(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, target: RecyclerView.ViewHolder?): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int) {
    }

    override fun convertToAbsoluteDirection(flags: Int, layoutDirection: Int): Int {

        if (swipeBack) {
            swipeBack = buttonShowedState != Button_canvas.GONE
            return 0
        }

        return super.convertToAbsoluteDirection(flags, layoutDirection)
    }

    override fun onChildDraw(c: Canvas?, recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
        var x = dX
        if (actionState == ACTION_STATE_SWIPE) {
            if (buttonShowedState != Button_canvas.GONE) {
                if (buttonShowedState == Button_canvas.RIGHT_VISIBLE) x = Math.min(x, -buttonWidth)
                super.onChildDraw(c, recyclerView, viewHolder, x, dY, actionState, isCurrentlyActive)
            } else {
                setTouchListener(c, recyclerView, viewHolder, x, dY, actionState, isCurrentlyActive)
            }

        }
        if (buttonShowedState == Button_canvas.GONE) {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        }
        currentItemViewHolder = viewHolder
    }

    private fun setTouchListener(c: Canvas?, recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
        recyclerView!!.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                swipeBack = event!!.action == MotionEvent.ACTION_CANCEL || event.action == MotionEvent.ACTION_UP
                if (swipeBack) {
                    if (dX < -buttonWidth) buttonShowedState = Button_canvas.RIGHT_VISIBLE

                    if (buttonShowedState != Button_canvas.GONE) {
                        setTouchDownListener(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                        setItemsClickable(recyclerView, false)
                    }

                }
                return false
            }

        })
    }

    private fun setTouchDownListener(c: Canvas?,recyclerView: RecyclerView?,viewHolder: RecyclerView.ViewHolder?,dX: Float,dY: Float,actionState: Int,isCurrentlyActive: Boolean){
        recyclerView!!.setOnTouchListener(object : View.OnTouchListener{
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {

                if(event!!.action == MotionEvent.ACTION_DOWN){
                    setTouchUpListener(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }

                return false
            }

        })
    }

    private fun setTouchUpListener(c: Canvas?, recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean){
        recyclerView!!.setOnTouchListener(object : View.OnTouchListener{
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {

                if(event!!.action == MotionEvent.ACTION_UP){
                    this@SwipeTouchDrawer.childDraw(c, recyclerView, viewHolder, 0f, dY, actionState, isCurrentlyActive)
                    recyclerView.setOnTouchListener(object : View.OnTouchListener{
                        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                            return false
                        }
                    })
                    setItemsClickable(recyclerView, true)
                    swipeBack = false

                    if (buttonsActions != null && buttonInstance != null && buttonInstance!!.contains(event.getX(), event.getY())) {

                        if (buttonShowedState == Button_canvas.RIGHT_VISIBLE) {
                            buttonsActions!!.onRightClicked(viewHolder!!.adapterPosition);
                        }
                    }
                    buttonShowedState = Button_canvas.GONE;
                    currentItemViewHolder = null;
                }
                return false
            }

        })
    }

    private fun childDraw(c: Canvas?,recyclerView: RecyclerView?,viewHolder: RecyclerView.ViewHolder?,dX: Float,dY: Float,actionState: Int,isCurrentlyActive: Boolean){
        super.onChildDraw(c,recyclerView,viewHolder,dX,dY,actionState,isCurrentlyActive)
    }

    private fun setItemsClickable(recyclerView: RecyclerView, isClickable: Boolean) {
        for (i in 0 until recyclerView.childCount) {
            recyclerView.getChildAt(i).isClickable = isClickable
        }
    }

    private fun drawButtons(c: Canvas, viewHolder: RecyclerView.ViewHolder) {
        val buttonWidthWithoutPadding = buttonWidth
        val corners = 0f

        val itemView = viewHolder.itemView
        val p = Paint()

        val deleteRect = RectF(itemView.right - buttonWidthWithoutPadding, itemView.top.toFloat(), itemView.right.toFloat(), itemView.bottom.toFloat())
        p.color = Color.parseColor("#E3263D")
        c.drawRoundRect(deleteRect, corners, corners, p)
        drawText("Delete", c, deleteRect, p)

        var drawable: Drawable = ContextCompat.getDrawable(context, R.drawable.ic_delete_icon)
        var bmp: Bitmap = drawableToBitmap(drawable)
        c.drawBitmap(bmp,deleteRect.centerX() - bmp.width/2 ,deleteRect.centerY() - (bmp.height/1.5).toFloat(),p)

        buttonInstance = null

       if (buttonShowedState == Button_canvas.RIGHT_VISIBLE) {
            buttonInstance = deleteRect
        }
    }

    private fun drawText(text: String, c: Canvas, button: RectF, p: Paint) {
        val textSize = 50f
        p.color = Color.WHITE
        p.isAntiAlias = true
        p.textSize = textSize
        val textWidth = p.measureText(text)
        c.drawText(text, button.centerX() - textWidth / 2, button.centerY() + (textSize * 2).toFloat() , p)
    }

    fun onDraw(c: Canvas) {
        if (currentItemViewHolder != null) {
            drawButtons(c, currentItemViewHolder!!)
        }
    }

    private fun drawableToBitmap(drawable: Drawable): Bitmap {

        if (drawable is BitmapDrawable) {
            return drawable.bitmap
        }

        val bitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)

        return bitmap
    }


}

internal enum class Button_canvas {
    GONE,
    RIGHT_VISIBLE
}
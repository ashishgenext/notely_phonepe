package notely.phonepe.notely.helper

import android.view.View

/**
 * Created by yoda on 3/2/18.
 */
interface OnListItemClickListener {
    fun onItemClick(view : View?,position:Int)
}